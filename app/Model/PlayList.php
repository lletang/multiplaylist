<?php 
class PlayList extends AppModel
{
    
    public $useTable = 'playlists';
    var $name = 'PlayList';


    var $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)

	);

	public $hasMany = 'Media';

    function apiFindById($id)
    {
    	return $this->find("first",array(
    		"conditions"=>array("PlayList.id"=>$id),
    		"fields"=>array("PlayList.*"),
    		"contain"=>array("Media.Plateforme")
    		));
    }
}