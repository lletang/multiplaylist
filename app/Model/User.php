<?php 

App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel
{

    public $useTable = 'users';
    public $name = 'User';

    public $validate = array(
        'login' => array(
            'alphanumeric' => array(
                'rule'     => 'alphaNumeric',
                'required' => true,
                'allowEmpty' => false,
                'message'  => 'Seul les chiffres et les lettres sont autorisés'
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'Ce login est déjà utilisé'
            )
        ),
        'email' => array(
            'email' => array(
            	'rule' => array('email'),
            	'required' => true,
            	'allowEmpty' => false,
            	'message' => 'L\'adresse email n\'est pas valide'
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'Cette adresse email est déjà utilisée'
            )
        ),  	
        'password' => array(
        	'rule' => array('between', 8, 15),
        	'required' => true,
        	'allowEmpty' => false,
            'message' => 'Le mot de passe doit avoir une longueur comprise entre 8 et 15 caractères.'
        )
    );

    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash($this->data[$this->alias]['password']);
        }
        return true;
    }

    public $belongsTo = array (
		'Role' => array (
			'className' => 'Role',
            'foreignKey' => 'role_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
		)
	);

    

	public $hasMany = 'PlayList';



	
}