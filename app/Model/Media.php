<?php 
class Media extends AppModel
{
    
    public $useTable = 'medias';
    var $name = 'Media';


    var $belongsTo = array(
		'PlayList' => array(
			'className' => 'PlayList',
			'foreignKey' => 'playlist_id'
		)
	);


}