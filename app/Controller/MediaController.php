<?php 

class MediaController extends AppController {

	function add(){
		$this->layout = 'ajax'; 
		$this->render(false);
		$data=array();

		$data["Media"]["playlist_id"] = $this->request["data"]["id_playlist"];
		$data["Media"]["titre"] = $this->request["data"]["media_title"];
		$data["Media"]["img_couverture"] = $this->request["data"]["media_img"];
		
		
		
		$idMedia = $this->request["data"]["media_id"];
		$data["Media"]["id_media"]=$idMedia;



		$plateforme = $this->request["data"]["media_plateforme"];


		$this->loadModel("Plateforme");
		$plateforme_ok = $this->Plateforme->find("first",array("conditions"=>array("libelle"=>$plateforme)));

		

		if(!isset($plateforme_ok) || empty($plateforme_ok))
			echo json_encode("{{'error':'plateforme incorrect'}}");
		else{
			if($plateforme=="youtube")
				$data["Media"]["url"] = $plateforme_ok["Plateforme"]["lien_api"].$idMedia;
			else 
				$data["Media"]["url"] = $this->request["data"]["url"];

			$data["Media"]["plateforme_id"] = $plateforme_ok["Plateforme"]["id"];

			
			$this->Media->create();
			$this->Media->save($data);
		}
	}

	function delete(){
		$this->layout = 'ajax'; 
		$this->render(false);
		if(!empty($this->request->params["id"])) {
			$this->Media->delete($this->request->params["id"]);
			$message["success"] = "deleted";
		}
		else {
			$message["error"] = "not deleted";
		}
		echo json_encode($message);
	}
}

