<?php 


class UsersController extends AppController {


    public function session() {

        $this->layout = 'ajax'; 
        $this->render(false);

        if ($this->request->is('post')) {
            $user = null;
            if($this->request->data["token"] != "NULL"){
                $user = $this->User->find('first', array('conditions' => array('User.token' => $this->request->data["token"])));
            }

            if(!empty($user)){
                $message["connected"] = "connected";
            }
            else {
                $message["noconnected"] = "noconnected";
            }

            echo json_encode($message);
        }


    }


	public function register() {

		$this->layout = 'ajax'; 
		$this->render(false);
       
        if ($this->request->is('post')) {

            $this->User->create();
            $this->User->set('role_id', 1);

            if ($this->User->save($this->request->data)) {               
                $message["success"] = "save";
                echo json_encode($message);
            } else {
            	$errors = $this->User->validationErrors;
            	echo json_encode($errors);
            }
        }

	}

    public function login() {
        //App::uses('AuthComponent', 'Controller/Component');
        App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
        $this->layout = 'ajax'; 
        $this->render(false);
        
        
        $passwordHasher = new BlowfishPasswordHasher();

        
        $user = $this->User->find('first', array(
        'conditions' => array('User.email' => $this->request->data["email"]),
        ));
        


        if(isset($user) && !empty($user)){


        
            if ($this->request->is('post')) {
                
                if ($passwordHasher->check($this->request->data["password"],$user["User"]["password"])) {
                    $message["success"] = "connected";
                    $t= strtotime(date("Y-m-d h:m:s")) ;
                    $token = password_hash ( $t, PASSWORD_BCRYPT);
                    $this->User->id = $user["User"]["id"];
                    $this->User->saveField("token",$token);
                    $message["token"]= $token;
                    $message["utilisateur_id"] = $user["User"]["id"];
                    $message["login"] = $user["User"]["login"];
                    echo json_encode($message);
                } else {
                    $message["error"] = "Email ou mot de passe incorrect";
                    echo json_encode($message);
                }
            }
             unset($user['User']['password']);  // don't forget this part
        }
        else{
            $message["error"] = "Email ou mot de passe incorrect";
            echo json_encode($message);
        }

    }

    public function isAllow(){

    }

    public function logout() {

        $this->layout = 'ajax'; 
        $this->render(false);
       
        $user = $this->User->find('first', array('conditions' => array('User.id' => $this->request->data["id"])));

        if(!empty($user)) {
            $this->User->id = $user["User"]["id"];
            $this->User->saveField("token", "null");
        }

    }

    public function update() {

        $this->layout = 'ajax'; 
        $this->render(false);
       
        $user = $this->User->find('first', array('conditions' => array('User.id' => $this->request->data["id"])));

        if(!empty($user)) {
            $this->User->id = $user["User"]["id"];
            if ($this->User->save($this->request->data)) {               
                $message["success"] = "save";
                $message["login"] = $this->request->data["login"];
                echo json_encode($message);
            } else {
                $errors = $this->User->validationErrors;
                echo json_encode($errors);
            }
        }

    }


}