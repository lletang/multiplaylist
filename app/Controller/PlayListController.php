<?php 

class PlayListController extends AppController {

	function search(){

		$id = $this->request->params["motcle"];
		$id = $this->request->params["motcle"];
		if($id!=NULL){

			$data_playlist = $this->PlayList->apiFindById($id);
			
			
			



		}
		else{

			$data_playlist = $this->PlayList->find("all");

			
		}
		
		echo json_encode($data_playlist);
		$this->layout = 'ajax'; 
		$this->render(false);
		

		
	}

	function playlists(){

		$this->layout = 'ajax'; 
		$this->render(false);

		
		if(isset($this->request->query["user_id"]) && $this->request->query["user_id"]!="undefined"){
			$data_playlist = $this->PlayList->find("all",array(
				"conditions"=>array(
					"user_id"=>$this->request->query["user_id"],
					"token"=>$this->request->query["token"]
				),
				"contain"=>""
			));
		}
		else if(isset($this->request->query["temporaire"]) && $this->request->query["temporaire"]!="undefined"){
			$data_playlist = $this->PlayList->find("all",array(
				"conditions"=>array(
					"temporaire"=>$this->request->query["temporaire"]
				),
				"contain"=>""
			));

		}
		else if(isset($this->request->params["id"]))
			$data_playlist = $this->PlayList->apiFindById($this->request->params["id"]);
		else if(empty($this->request->query))
			$data_playlist = $this->PlayList->find("all");
		else
			return false;

		
		echo json_encode($data_playlist);
		
		

	}

	function add(){

		
		
		if($this->request["data"]["temporaire"]!="NULL" && $this->request["data"]["temporaire"]!="undefined"){
			$d["PlayList"]["temporaire"] = $this->request["data"]["temporaire"];

			$data = array("temporaire_id"=>$this->request["data"]["temporaire"]);
		}

		if($this->request["data"]["user_id"]!="NULL" && $this->request["data"]["user_id"]!="undefined"){
			$d["PlayList"]["user_id"] = $this->request["data"]["user_id"];

			$data = array("utilisateur_id"=>$this->request["data"]["user_id"]);
		}
		else if( ( $this->request["data"]["user_id"]=="NULL" || $this->request["data"]["user_id"]=="undefined") && ($this->request["data"]["temporaire"]=="NULL" || $this->request["data"]["temporaire"]=="undefined" )){
			$tmp = strtotime(date("Y-m-d h:m:s")) ;
			$tmp = hash("sha256", $tmp);

			
			$d["PlayList"]["temporaire"] = $tmp;

			$data = array("temporaire_id"=>$tmp);
		}

		$d["PlayList"]["libelle"] = $this->request["data"]["nom"];

		
		$this->PlayList->create();
		if($this->PlayList->save($d)){
			$data["id"]= $this->PlayList->id;
			echo json_encode($data);
		}
		else{
			echo json_encode(array("error"=>"une erreur est surevenue"));
		}




		
		
		$this->layout = 'ajax'; 
		$this->render(false);
	}


	function privacy(){
		$this->layout = 'ajax'; 
		$this->render(false);
		if(isset($this->request->params["position"]) && isset($this->request->params["idPL"])){

			$id_playlist = $this->request->params["idPL"];
			$position = $this->request->params["position"];
			$this->PlayList->id = $id_playlist;

			$this->PlayList->saveField("privacy",$position);

			echo  json_encode(array("position"=>$position));



		}


	}

	

}
