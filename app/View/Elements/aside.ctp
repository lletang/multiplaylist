<!-- .aside -->
        <aside class="bg-black dk  aside hidden-print" id="nav">          
          <section class="vbox">
            <section class="w-f-md scrollable" 	>
              <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-railOpacity="0.2" >
                


                <!-- nav -->                 
                <nav class="nav-primary hidden-xs">
                  <ul class="nav bg clearfix">
                    <li class="hidden-nav-xs padder m-t m-b-sm text-xs text-muted">
                      Menu
                    </li>
                    <li>
                      <a href="/#/search">
                        <i class="fa fa-search icon text-success"></i>
                        <span class="font-bold">Rechercher</span>
                      </a>
                    </li>
                    <li>
                      <a href="/#/news">
                        <i class="icon-music-tone-alt icon text-info"></i>
                        <span class="font-bold">Playlists</span>
                      </a>
                    </li>
                    <li>
                      <a style="cursor:pointer" ><div ng-controller="showPlayListController">  

                        <span ng-click="add()" ><i class="icon-plus i-lg text-info-dker" ></i>
                        
                        <span class="font-bold">Créer une playlist</span></span>
                        </div>
                      </a>
                    </li>
                   
                    <li class="m-b hidden-nav-xs"></li>
                  </ul>
                  
                  <div id="appShowPlayList">
                  	 <div ng-controller="showPlayListController">  
                  	 	   <ul class="nav text-sm" style="margin-bottom: 60px;margin-top:10px">
						              <li class=" padder m-t m-b-sm text-xs text-muted">
						               
						                <span class="hidden-nav-xs" >Playlist</span>
						              </li>
						              <div ng-repeat="pl in playlists track by $index" class="list-group">
							  
  							             <li style="" class="">

  						    	           <a href="/#/listeLecture/{{pl.PlayList.id}}" class="list-group-item">
    						      		       <i class="icon-music-tone icon"></i>
    						      		       <b class="badge bg-success dker pull-right" style="margin-right:10px">{{pl.Media.length}}</b>
    						      		       <span>{{pl.PlayList.libelle}}</span>
  						    	           </a>
  						  	           </li>
                             

						              </div>
						              
						  
						  
						            </ul>
				       
				              </div>
                  	<!--<div ng-app="showPlayListApp" ng-view></div>-->
                  </div>
                  
                </nav>
                <!-- / nav -->
              </div>
            </section>
            

          </section>
</aside>