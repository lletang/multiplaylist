<?php 

  $cakeDescription = __d('cake_dev', 'Time to listen');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())

?>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>


  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

  <link rel="stylesheet" href="/js/lib/jPlayer/jplayer.flat.css" type="text/css" />

  <link rel="stylesheet" href="/css/animate.css" type="text/css" />
  <link rel="stylesheet" href="/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="/css/simple-line-icons.css" type="text/css" />
  <link rel="stylesheet" href="/css/font.css" type="text/css" />
  <link rel="stylesheet" href="/css/app.css" type="text/css" />  
  <link rel="stylesheet" href="js/lib/nestable/nestable.css" type="text/css" />
  <link rel="stylesheet" href="/css/ngDialog.css">
	<link rel="stylesheet" href="/css/ngDialog-theme-default.css">
	<link rel="stylesheet" href="/css/ngDialog-theme-plain.css"> 
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
 
  <script type="text/javascript" src="js/lib/soundcloud/soundcloud.player.api.js"></script>
  <script type="text/javascript" src="http://www.youtube.com/player_api"></script>





	<?php
		echo $this->Html->meta('icon');

		
		echo $this->Html->css('angular');
		echo '<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">';


		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
		
		//echo $this->Html->script('lib/angular/angular-animate.min'); 
		echo $this->Html->script('lib/angular/angular.min'); 
    echo $this->Html->script('lib/angular/angular-route.min'); 
    echo $this->Html->script('lib/angular/angular-sanitize.min'); 
    echo $this->Html->script('lib/angular/angular-resource.min'); 
    echo $this->Html->script('lib/angular/ngDialog.min'); 
   
    echo $this->Html->script('angularApp/js/timeToListenApp');
    
    echo $this->Html->script('angularApp/js/factory/searchFactory');
    echo $this->Html->script('angularApp/js/controller/homeController');
    
    echo $this->Html->script('angularApp/js/controller/searchController');
    echo $this->Html->script('angularApp/js/controller/showPlayListController');
    echo $this->Html->script('angularApp/js/controller/listeLectureController');
    echo $this->Html->script('angularApp/js/controller/playController');
    echo $this->Html->script('angularApp/js/controller/userController');
        

	?>

  <script src="http://connect.soundcloud.com/sdk.js"></script>



</head>