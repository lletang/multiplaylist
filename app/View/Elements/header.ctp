<header class="bg-white-only header header-md navbar navbar-fixed-top-xs" >
      <div class="navbar-header aside bg-info " >
        <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen,open" data-target="#nav,html">
          <i class="icon-list"></i>
        </a>
        <a href="/#/" class="navbar-brand text-lt">
          <span class="hidden-nav-xs m-l-sm">Time To Listen</span>
        </a>
        <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user">
          <i class="icon-settings"></i>
        </a>
      </div>      
      <ul class="nav navbar-nav hidden-xs">
        <li>
          <a href="#nav,.navbar-header" data-toggle="class:nav-xs,nav-xs" class="text-muted">
            <i class="fa fa-indent text"></i>
            <i class="fa fa-dedent text-active"></i>
          </a>
        </li>
      </ul>
      
      <div class="navbar-right ">
        <ul class="nav navbar-nav m-n hidden-xs nav-user user">
        <!-- Profile -->
          <li><span ng-model="nomUtilisateur" style="padding: 20px;position: relative;top: 20px"> {{nomUtilisateur}}</span></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle bg clear" data-toggle="dropdown"> <b>Inscription / Connexion</b><b class="caret"></b></a>
            <ul class="dropdown-menu animated fadeInRight">            
              <li ng-if="!session">
                <span class="arrow top"></span>
                <a href="/#/register">Inscription</a>
              </li>
              <li class="divider" ></li>
              <li ng-if="!session">
                <a href="/#/login">Connexion</a>
              </li>
              <li class="divider" ></li>
              <li ng-if="session"> 
                <a href="/#/profil">Profil</a>
              </li>
              <li ng-if="session" class="divider"></li>
              <li>     
                <a ng-if="session" style="cursor:pointer" ><span ng-controller="userController" ng-click="logout()">Déconnexion</span></a>
              </li>
            </ul>
          </li>
        <!-- End Profile -->
        </ul>
      </div>      
    </header>