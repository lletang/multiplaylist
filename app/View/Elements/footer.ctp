<footer class="footer bg-dark" style="position:fixed;bottom:0">
              <div ng-controller="playController">  
                  <div id="jp_container_N">
                    <div class="jp-type-playlist">
                      <div id="jplayer_N" class="jp-jplayer hide"></div>
                      <div class="jp-gui">
                        <div class="jp-video-play hide">
                          <a class="jp-video-play-icon">play</a>
                        </div>
                        <div class="jp-interface">
                          <div class="jp-controls">
                            <div><a class="jp-previous" ng-click="prev()"><i class="icon-control-rewind i-lg"></i></a></div>
                            <div>
                              
                                <a class="jp-play" ng-click="play()"><i class="icon-control-play i-2x"></i></a>
                                <a class="jp-pause hid" ng-click="pause()"><i class="icon-control-pause i-2x"></i></a>
                              
                            </div>
                            <div><a class="jp-next" ng-click="next()"><i class="icon-control-forward i-lg"></i></a></div>
                            <div class="hide"><a class="jp-stop"><i class="fa fa-stop"></i></a></div>
                            <div><a class="" data-toggle="dropdown" data-target="#playlist"><i class="icon-list"></i></a></div>
                            <div class="jp-progress hidden-xs">
                              <div class="jp-seek-bar dk">
                                <div class="jp-play-bar bg-info">
                                </div>
                                <div class="jp-title text-lt">
                                  <ul>
                                    <li>{{titreEnCours}}</li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            
                            
                            <div class="hidden-xs hidden-sm" style="text-decoration: none;">
                              <a class="jp-mute" title="mute"><i class="icon-volume-2"></i></a>
                              <a class="jp-unmute hid" title="unmute"><i class="icon-volume-off"></i></a>
                            </div>
                            <div class="hidden-xs hidden-sm jp-volume">
                            <div ng-controller="playController" class="jp-volume-bar dk" style="max-width: 50%;">
                              <form>
                              <input class="jp-volume-bar-value lter" type="range" ng-model="volume" ng-click="setSound()" id="volume-sound">
                              </form>
                            </div>
                              
                            </div>
                            
                            
                          </div>
                        </div>
                      </div>
                      <div class="jp-playlist dropup" id="playlist">
                        <ul class="dropdown-menu aside-xl dker">
                          <!-- The method Playlist.displayPlaylist() uses this unordered list -->
                            <div ng-controller="playController">
                              <div ng-repeat="pl in playListeEnCours.Media">
                                  <li class="list-group-item">{{pl.titre}} </li>
                              </div>
                              
                            </div>



                          
                          
                        </ul>
                      </div>
                      <div class="jp-no-solution hide">
                        <span>Update Required</span>
                        To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                      </div>
                    </div>
                  </div>
                </div>
</footer>