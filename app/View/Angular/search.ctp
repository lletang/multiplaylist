<section class="vbox">
    <!--header-->
    <?php echo $this->element('header'); ?>
    <section>
      <section class="hbox stretch">
        <!-- ASIDE -->
        <?php echo $this->element('aside'); ?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder-lg w-f-md" id="bjax-target">
                  <!-- <a href="#" class="pull-right text-muted m-t-lg" data-toggle="class:fa-spin" ><i class="icon-refresh i-lg  inline" id="refresh"></i></a> -->

				  
                  


<form class="navbar-form navbar-left input-s-lg m-t m-l-n-xs" role="search" style="width: 100%">
       
            <h1>Rechercher une musique</h1>
            <input type="text" class="form-control input-sm no-border rounded" ng-model="motcle" style="width:80%;height:50px" placeholder="Search media,vidéo,music">
            
            
            <div class="input-group-btn" style="display:block;margin-top:10px;" >
              <button type="submit" style="width: 120px;" class="btn btn-lg btn-danger btn-icon rounded" ng-click="RechercheMedia('youtube')"><i class="fa fa-search"></i>Youtube</button>
              <button type="submit" style="width: 120px;margin-left:20px" class="btn btn-lg btn-warning btn-icon rounded" ng-click="RechercheMedia('soundcloud')"><i class="fa fa-search"></i>Soundcloud</button>
              
            </div>
            
          
        
</form>


<div ng-repeat="media in medias_youtube" >
  <div class="col-xs-6 col-sm-3" ng-click="open(media.snippet.title,media.id.videoId,media.snippet.thumbnails.high.url)" 
  style="cursor:pointer">
      <div class="item">
          <div class="pos-rlt">
              <div class="item-overlay opacity r r-2x bg-black">
                  <div class="center text-center m-t-n">
                      <div ><i class="fa  fa-plus i-2x"></i></div>
                  </div>
              </div>
              <a href="#"><img src="{{media.snippet.thumbnails.high.url}}" alt="" width="383"  class="r r-2x img-full"></a>
          </div>
          <div class="padder-v">
              <div class="text-ellipsis">{{media.snippet.title}}</div>
          </div>
      </div>
  </div>
</div>


<div ng-repeat="media in medias_soundcloud" >
    <div class="col-xs-6 col-sm-3" ng-click="open(media.title,media.id,media.artwork_url,media.permalink_url)" style="cursor:pointer">
        <div class="item">
            <div class="pos-rlt">
                <div class="item-overlay opacity r r-2x bg-black">
                    <div class="center text-center m-t-n">
                        <div ><i class="fa  fa-plus i-2x"></i></div>
                    </div>
                </div>
                <div ng-if="media.artwork_url == null ">
          			   <a href="#"><img width="383"  src="http://nancyharmonjenkins.com/wp-content/plugins/nertworks-all-in-one-social-share-tools/images/no_image.png" alt="" class="r r-2x img-full"></a>
       			    </div>
       			    <div ng-if="media.artwork_url != null ">
          			   <a href="#"><img width="383"  src="{{media.artwork_url}}" alt="" class="r r-2x img-full"></a>
       			    </div>
                
            </div>
            <div class="padder-v">
                <div class="text-ellipsis">{{media.title}}</div>
                
            </div>
        </div>
    </div>
    
</div>



<script type="text/ng-template" id="firstDialogId">
		<div class="ngdialog-message">
			<h3>Liste de vos playlists disponibles : </h3>
			<select ng-model="playlist_id" class="form-control" >
				<option value="">Choisissez une de vos playlists</option>
			 	<option ng-repeat="playlist in playlists" value="{{playlist.PlayList.id}}">{{playlist.PlayList.libelle}}</option>
			</select>
			
		</div>
    <input type="hidden" name="id" value="{{id}}" />
		<div class="ngdialog-buttons">
			<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="addMedia()">Ajouter le media</button>
			
		</div>
		
</script>
                  
                  
                  
                </section>
                <?php echo $this->element('footer'); ?>
                <!-- FOOTER -->
              </section>
            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen,open" data-target="#nav,html"></a>
        </section>
      </section>
    </section>    
  </section>







