<section class="vbox">
    <!--header-->
    <?php echo $this->element('header'); ?>
    <section>
      <section class="hbox stretch">
        <!-- ASIDE -->
        <?php echo $this->element('aside'); ?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox" style="width: 100%; overflow: auto; height: 55%; margin-bottom: 15%">
                <section class="scrollable padder-lg w-f-md">    
                  <div class="container">
                      <h1>Editer mon profil</h1>
                      <hr>
                    <div class="row">                        
                        <!-- edit form column -->
                        <div ng-if="success" >
                          <div class="alert alert-success alert-dismissible" role="alert" style="max-width: 75%;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;
                            </span></button>
                            Votre profil à été modifié
                          </div>
                        </div>
                        <div class="col-md-9 personal-info">                      
                          <form novalidate class="form-horizontal" name="editForm" role="form" style="max-width: 75%;">
                            <div class="form-group" ng-class="{'has-error': editForm.login.$dirty && editForm.login.$invalid, 
                              'has-success': editForm.login.$valid}">
                              <div class="col-sm-9" style="text-align: center;" class="text-danger">     
                                <div ng-show="errorLogin" class="text-danger"> {{errorLogin}} </div>                 
                                <div class="text-danger" ng-show="editForm.login.$invalid"> 
                                  <div ng-show="editForm.login.$error.minlength">Minimum 3 caractères</div>
                                  <div ng-show="editForm.login.$error.pattern">Format du login incorrect </div>
                                </div>
                              </div><br/><br ng-if="errorLogin" />
                              <label class="col-sm-2 control-label" for="login">Login</label>
                              <div class="col-sm-9"> 
                                <input ng-pattern="/^[a-zA-Z0-9]+$/" ng-minlength="3" class="form-control" type="text" id="login" name="login" ng-model="user.login">
                              </div>                    
                            </div><br/>

                              <div class="form-group" ng-class="{'has-error': editForm.email.$dirty && editForm.email.$invalid, 
                                'has-success': editForm.email.$valid}">
                                <div class="col-sm-9" style="text-align: center;" class="text-danger">     
                                  <div ng-if="errorEmail" class="text-danger"> {{errorEmail}} </div>                 
                                  <div class="text-danger" ng-show="editForm.email.$invalid"> 
                                    <div ng-show="editForm.email.$error.email">L'adresse email n'est pas valide</div>
                                  </div>
                                </div><br/><br ng-if="errorEmail"/>
                                <label class="col-sm-2 control-label" for="email">Email</label> 
                                <div class="col-sm-9"> 
                                  <input class="form-control" type="email" id="email" name="email" ng-model="user.email">
                                </div>                      
                              </div><br/>

                              <div class="form-group pull-in clearfix" ng-class="{'has-error': editForm.password.$dirty && editForm.password.$invalid, 'has-success': editForm.password.$valid}">
                                <div class="col-sm-9" style="text-align: center;" class="text-danger">     
                                  <div ng-if="errorPassword" class="text-danger"> {{errorPassword}} </div>                 
                                  <div class="text-danger" ng-show="editForm.password.$invalid"> 
                                    <div ng-show="editForm.password.$error.minlength">Minimum 8 caractères</div>
                                    <div ng-show="editForm.password.$error.maxlength">Maximum 15 caractères</div>
                                  </div>
                                </div><br/><br ng-if="errorPassword"/>
                                <label class="col-sm-2 control-label" for="password">Mot de passe</label> 
                                <div class="col-sm-9">
                                  <input class="form-control" ng-minlength="8" ng-maxlength="15" id="password" type="password" name="password" ng-model="user.password">   
                                </div>
                              </div>

                            <div class="form-group">
                              <label class="col-md-3 control-label"></label>
                              <div class="col-md-8">
                                <input class="btn btn-primary" value="Sauvegarder" ng-click="editProfil(user.$valid)" type="submit">
                                <input style="margin: 10px;" class="btn btn-default" value="Reset" type="reset">
                              </div>
                            </div>
                          </form>
                        </div>
                    </div>
                  </div>
                  <hr>
                </section>
                <?php echo $this->element('footer'); ?>
                <!-- FOOTER -->
              </section>
            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen,open" data-target="#nav,html"></a>
        </section>
      </section>
    </section>    
  </section>







