<section class="vbox">
    <!--header-->
    <?php echo $this->element('header'); ?>
    <section>
      <section class="hbox stretch">
        <!-- ASIDE -->
        <?php echo $this->element('aside'); ?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder-lg w-f-md" id="bjax-target">
                  <section class="hbox stretch">
                    <section>
                      <section class="vbox" style="overflow: auto; height: 62%; margin-bottom: 14%;">
                        <div class="post-item" style="box-shadow: 8px 8px 12px #aaa; border:1px solid #E0E0E0 ;">
                      <div class="post-media">
                        <H1>Bienvenue sur TimeToListen</H1>
                        <img src="images/logo.jpg" style="max-width:10%;">
                      </div>
                      <div class="caption wrapper-lg">
                        <div class="post-sum">
                          <p>TimeToListen est un site d'écoute de musiques en ligne vous permettant de créer des playlists mélangeant des musiques provenant de diverses plateformes. 
                          <br><br>
                          Vous pourrez ainsi créer vos playlists personnalisées sans être obligés de changer de page. Adieu les changements d'onglets, toutes vos musiques se retrouvent sur une même page !
                        </div>
                      </div>
                      </section>
                    </section>
                  </section>
                </section>
                <?php echo $this->element('footer'); ?>
                <!-- FOOTER -->
              </section>
            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen,open" data-target="#nav,html"></a>
        </section>
      </section>
    </section>    
  </section>






























