<section class="vbox">
    <!--header-->
    <?php echo $this->element('header'); ?>
    <section>
      <section class="hbox stretch">
        <!-- ASIDE -->
        <?php echo $this->element('aside'); ?>
        <!-- /.aside -->
        <section id="content">
          <section class="bg-dark">
            <div  class="carousel slide carousel-fade panel-body" id="c-fade" style="height:240px;">
                <div data-ride="carousel" data-interval="4000" class="carousel-inner">
                  <div class="item" ng-repeat="media in PlayListPage.Media">
                    <p class="text-center" style="background-color:#2C3942;">
                      <img src="{{media.img_couverture}}" alt="Pas d'image disponible" style="height:180px;"><br>{{media.titre}}
                    </p>
                  </div>
                  <div class="item active">
                    <p class="text-center">
                      <em class="h2 text-mute"><br/>Playlist <b>{{PlayListPage.PlayList.libelle}}</b></em><br>
                    </p>
                  </div>
                </div>
                <a class="left carousel-control" href="#c-fade" data-slide="prev">
                  <i class="fa fa-angle-left"></i>
                </a>
                <a class="right carousel-control" href="#c-fade" data-slide="next">
                  <i class="fa fa-angle-right"></i>
                </a>
            </div>
          </section> 
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable  w-f-md" id="bjax-target" >              
                <section class="vbox" style="overflow: auto; height: 55%; margin-bottom: 15%" >
                  <section class="w-f-md">
                    <section class="hbox stretch bg-black dker">
                      <!-- side content -->
                      <aside class="col-sm-5 no-padder" id="sidebar">
                        <section class="vbox animated fadeInUp">
                          <section class="scrollable">
                            <div class="m-t-n-xxs item pos-rlt">
                              <div class="top text-right">
                              </div>
                              <div class="bottom gd bg-info wrapper-lg" style="position:relative">
                                <span class="h2 font-thin" ng-model="PlayListPage">{{PlayListPage.PlayList.libelle}}</span>
                                <div ng-controller="playController">
                                  <a class="jp-play" ng-click="play(idPL)"><i class="icon-control-play i-2x"></i></a>
                                </div>
                                <div style="float:right">
                                <label class="switch">
                                  <input type="checkbox" id="checkBoxPrivacy" ng-click="changePrivacy(idPL)">
                                </label>
                              </div>
                              </div>
                            </div>
                            <ul class="list-group list-group-lg no-radius no-border no-bg m-t-n-xxs m-b-none auto">
                              <li class="list-group-item" ng-repeat="media in listMedia track by $index">
                                <div class="pull-right m-l" ng-click="listMedia.splice($index,1)">
                                  <a ng-click="deleteMusic(media)"><i class="icon-close"></i></a>
                                </div>
                                <a style="text-decoration: none;" href="#" class="pull-left thumb-sm m-r">
                                  <img style="border-radius: 20px;" src="{{media.img_couverture}}" alt="...">
                                </a>
                                <div class="clear text-ellipsis">
                                  <span>{{media.titre}}</span>
                                </div>
                              </li>
                            </ul>
                          </section>
                        </section>
                      </aside>
                    </section>
                  </section>
                </section>
              </section>
                <?php echo $this->element('footer'); ?>
                <!-- FOOTER -->
              </section>
            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen,open" data-target="#nav,html"></a>
        </section>
      </section>
    </section>    
</section>
