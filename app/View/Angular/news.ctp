<section class="vbox">
    <!--header-->
    <?php echo $this->element('header'); ?>
    <section>
      <section class="hbox stretch">
        <?php echo $this->element('aside'); ?>
        <!-- /.aside -->
        <section id="content">

          <section class="vbox">
            <section class="w-f-md">
              <section class="hbox stretch bg-black dker">  

                  <section class="col-sm-4 no-padder bg" >
                    <header class="panel-heading m-xs">
                      <div style="float: right; padding: 9px;" class="badge bg-info">Nombre de playlist : {{objects.length}}</div>
                      <div class="input-group text-sm">
                        <input class="input-sm form-control" placeholder="Recherche une playlist" type="text" ng-model="q" >
                      </div>
                    </header>
                    <section class="vbox" style="overflow: auto; height: 45%; margin-bottom: 14%;">

                      <section class="scrollable hover">
                        <ul class="list-group list-group-lg no-bg auto m-b-none m-t-n-xxs adtm" ng-controller="playController">
                          <li class="list-group-item clearfix" ng-repeat="object in objects | filter:q as results">
                            <a ng-click="play(object.PlayList.id)" style="text-decoration: none;" href="#" class="jp-play-me pull-right m-t-sm m-l text-md">
                              <i class="icon-control-play text"></i>
                              <i class="icon-control-pause text-active"></i>
                            </a>                    

                            <button style="float: right;" class="btn btn-primary" type="button" data-toggle="collapse" 
                              data-target="#{{object.PlayList.id}}" aria-expanded="false" aria-controls="{{object.PlayList.id}}">
                              Musiques <span class="badge">{{object.Media.length}}</span>
                            </button>
                            </span>

                            <a ng-repeat="media in object.Media" style="text-decoration: none;" href="#" class="pull-left thumb-sm m-r">
                              <img ng-if="$first" style="border-radius: 20px;" src="{{media.img_couverture}}" alt="...">
                            </a>
                            <a style="text-decoration: none;" class="clear" href="#">
                              <span class="block text-ellipsis"><b>{{object.PlayList.libelle}}</b></span>

                              <small class="text-muted">Crée par 
                                <span class="lastfirstname">{{object.User.login}}</span>
                              </small>
                            </a>
                            <ul class="list-group collapse" id="{{object.PlayList.id}}" style="margin-top: 10px;">
                              <li class="list-group-item" ng-repeat="media in object.Media" style="border: 1px solid #2E2E2E !important;">
                                <a style="text-decoration: none;" href="#" class="pull-left thumb-sm m-r">
                                  <img style="border-radius: 20px;" src="{{media.img_couverture}}" alt="...">
                                </a>
                                <a style="text-decoration: none;" class="clear" href="#">
                                  <span class="block text-ellipsis">{{media.titre}}</span>
                                </a>
                              </li>
                            </ul>

                          </li>
                          <li style="margin: 5px;" class="list-group-item clearfix" ng-if="results.length == 0">
                            <strong>Aucun résultat trouvé pour cette recherche</strong>
                          </li>
                        </ul>
                      </section>
                    </section>
                  </section>
                </section>
              </section>
            <!-- FOOTER -->
            <?php echo $this->element('footer'); ?>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen,open" data-target="#nav,html"></a>
        </section>
      </section>
    </section>    
  </section>







