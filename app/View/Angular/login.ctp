<section class="vbox">
    <!--header-->
    <?php echo $this->element('header'); ?>
    <section>
      <section class="hbox stretch">
        <!-- ASIDE -->
        <?php echo $this->element('aside'); ?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder-lg w-f-md" id="bjax-target">
                  <br/>
                  <form novalidate name="loginForm" style="max-width: 80%; margin: auto; margin-bottom: 19%;">
                    <section class="panel panel-default">
                      <header class="panel-heading">
                        <span class="h4">Connexion</span>
                      </header>
                      <div class="panel-body">   
                      <div class="col-sm-9" style="text-align: center;" class="text-danger" style="text-align: center;">
                        <div ng-show="errorEmail" class="text-danger"> {{errorEmail}} </div>
                      </div>
                        <br/><br/>
                        <label class="col-sm-2 control-label" for="email">Email</label> 
                        <div class="col-sm-9"> 
                          <input class="form-control" required type="email" id="email" name="email" ng-model="user.email">
                        </div>                      
                        <br/><br/>
                        <label class="col-sm-2 control-label" for="password">Mot de passe</label> 
                        <div class="col-sm-9">
                          <input class="form-control" required id="password" type="password" name="password" ng-model="user.password">   
                        </div>
                      </div>
                      <footer class="panel-footer text-right lter footerRegister">
                        <input value="Valider" ng-click="login()" type="submit" class="btn btn-success btn-s-xs">
                      </footer>
                    </section>
                  </form>
                </section>
                <?php echo $this->element('footer'); ?>
                <!-- FOOTER -->
              </section>
            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen,open" data-target="#nav,html"></a>
        </section>
      </section>
    </section>    
  </section>







