<section class="vbox">
    <!--header-->
    <?php echo $this->element('header'); ?>
    <section>
      <section class="hbox stretch">
        <!-- ASIDE -->
        <?php echo $this->element('aside'); ?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder-lg w-f-md" id="bjax-target">    
                <br />
                <!-- FORMULAIRE -->
                <form novalidate name="registerForm" style="max-width: 80%; margin: auto; margin-bottom: 19%;">
                  <section class="panel panel-default">
                    <header class="panel-heading">
                      <span class="h4">Inscription</span>
                    </header>
                    <div class="panel-body">
                      <p class="text-muted">Veuillez remplir tout les champs</p>
                      <div class="form-group" ng-class="{'has-error': registerForm.login.$dirty && registerForm.login.$invalid, 
                        'has-success': registerForm.login.$valid}">
                        <div class="col-sm-9" style="text-align: center;" class="text-danger">     
                          <div ng-show="errorLogin" class="text-danger"> {{errorLogin}} </div>                 
                          <div class="text-danger" ng-show="registerForm.login.$invalid"> 
                            <div ng-show="registerForm.login.$error.required">Champ obligatoire</div>
                            <div ng-show="registerForm.login.$error.minlength">Minimum 3 caractères</div>
                            <div ng-show="registerForm.login.$error.pattern">Format du login incorrect </div>
                          </div>
                        </div><br/><br ng-if="errorLogin" />
                        <label class="col-sm-2 control-label" for="login">Login</label>
                        <div class="col-sm-9"> 
                          <input ng-pattern="/^[a-zA-Z0-9]+$/" ng-minlength="3" class="form-control" required type="text" id="login" name="login" ng-model="user.login">
                        </div>                    
                      </div><br/>
                      <div class="form-group" ng-class="{'has-error': registerForm.email.$dirty && registerForm.email.$invalid, 
                        'has-success': registerForm.email.$valid}">
                        <div class="col-sm-9" style="text-align: center;" class="text-danger">     
                          <div ng-if="errorEmail" class="text-danger"> {{errorEmail}} </div>                 
                          <div class="text-danger" ng-show="registerForm.email.$invalid"> 
                            <div ng-show="registerForm.email.$error.required">Champ obligatoire</div>
                            <div ng-show="registerForm.email.$error.email">L'adresse email n'est pas valide</div>
                          </div>
                        </div><br/><br ng-if="errorEmail"/>
                        <label class="col-sm-2 control-label" for="email">Email</label> 
                        <div class="col-sm-9"> 
                          <input class="form-control" required type="email" id="email" name="email" ng-model="user.email">
                        </div>                      
                      </div><br/>
                      <div class="form-group pull-in clearfix" ng-class="{'has-error': registerForm.password.$dirty && registerForm.password.$invalid, 'has-success': registerForm.password.$valid}">
                        <div class="col-sm-9" style="text-align: center;" class="text-danger">     
                          <div ng-if="errorPassword" class="text-danger"> {{errorPassword}} </div>                 
                          <div class="text-danger" ng-show="registerForm.password.$invalid"> 
                            <div ng-show="registerForm.password.$error.required">Champ obligatoire</div>
                            <div ng-show="registerForm.password.$error.minlength">Minimum 8 caractères</div>
                            <div ng-show="registerForm.password.$error.maxlength">Maximum 15 caractères</div>
                          </div>
                        </div><br/><br ng-if="errorPassword"/>
                        <label class="col-sm-2 control-label" for="password">Mot de passe</label> 
                        <div class="col-sm-9">
                          <input class="form-control" ng-minlength="8" ng-maxlength="15" required id="password" type="password" name="password" ng-model="user.password">   
                        </div>
                    </div>
                    <footer class="panel-footer text-right lter footerRegister">
                      <input value="Valider" ng-click="register(user.$valid)" type="submit" class="btn btn-primary btn-s-xs">
                    </footer>
                    </div>
                  </section>
                </form>
                <!-- FORMULAIRE -->
                </section>
                <?php echo $this->element('footer'); ?>
                <!-- FOOTER -->
              </section>
            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen,open" data-target="#nav,html"></a>
        </section>
      </section>
    </section>    
  </section>







