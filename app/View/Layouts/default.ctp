<?php


?>
<!DOCTYPE html>
<html class="app">
<?php echo $this->element('head'); ?>
<!-- HEAD-->
<body class="">
	<div ng-app="timetolistenApp" ng-view></div>
  <?php echo $this->Session->flash(); ?>

  <?php echo $this->fetch('content'); ?>
	
	<script src="http://connect.soundcloud.com/sdk.js"></script>
	
		<!-- Bootstrap JavaScript -->
	
  <script src="/js/lib/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="/js/lib/bootstrap.min.js"></script>
  <!-- App -->
  <script src="/js/lib/parsley/parsley.min.js"></script>
  <script src="/js/lib/parsley/parsley.extend.js"></script>
  <script src="js/lib/app.js"></script>  
  <script src="js/lib/slimscroll/jquery.slimscroll.min.js"></script>
  <script src="js/lib/app.plugin.js"></script>
  <script type="text/javascript" src="js/lib/jPlayer/jquery.jplayer.min.js"></script>
  <script type="text/javascript" src="js/lib/jPlayer/add-on/jplayer.playlist.min.js"></script>
  <script type="text/javascript" src="js/lib/jPlayer/demo.js"></script>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>

