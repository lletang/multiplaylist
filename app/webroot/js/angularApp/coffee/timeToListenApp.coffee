app = angular.module( 'timetolistenApp', ['ngRoute','ngResource','ngDialog'])

app.config(['$routeProvider',($routeProvider)->
		$routeProvider.when('/',{
			templateUrl: "/angular/accueil",
			controller: "homeController"	
		})
		$routeProvider.when('/add',{
			templateUrl: "/angular/addPlayList",
			controller: "addPlayListController"	
		})
		$routeProvider.when('/search',{
			templateUrl: "/angular/search",
			controller: "searchController"	
		})
		$routeProvider.when('/listeLecture/:id',{
			templateUrl: "/angular/listeLecture",
			controller: "listeLectureController"	
		})
		$routeProvider.when('/register',{
			templateUrl: "/angular/register",
			controller: "userController"	
		})
		$routeProvider.when('/login',{
			templateUrl: "/angular/login",
			controller: "userController"	
		})
		$routeProvider.when('/profil',{
			templateUrl: "/angular/profil",
			controller: "userController"	
		})
		$routeProvider.when('/news',{
			templateUrl: "/angular/news",
			controller: "showPlayListController"	
		})
		$routeProvider.otherwise({
        	redirectTo: '/'
      	})
		

		
	]
)

app.config(['ngDialogProvider', (ngDialogProvider)->
			ngDialogProvider.setDefaults({
				className: 'ngdialog-theme-default',
				plain: false,
				showClose: true,
				closeByDocument: true,
				closeByEscape: true,
				appendTo: false,
				preCloseCallback: ()-> 
					return true
				
			});
		]);

# Callback à utiliser en cas d'erreur côté serveur
window.serverErrorHandler = ->
	alert("Une erreur s'est produite sur le serveur, veuillez réessayer")



