angular.module('timetolistenApp').factory 'searchFactory', ['$routeParams',
  '$http'
  ($routeParams,$http) ->
    
    urlYoutube = '/api/youtube/search/'

    urlSoundCloud = '/api/soundcloud/search/'
    urlPlaylist = '/api/playlists/'

    dataFactory = {}
    dataFactory.getYoutube = (motcle)->
      url = urlYoutube+motcle
      $http.get url
    dataFactory.getSoundcloud = (motcle)->
      url = urlSoundCloud+motcle
      $http.get url

    dataFactory.getListPlayList = ()-> 
      $http.get urlPlaylist
    dataFactory.getOnePlayList = (id)-> 
      url = urlPlaylist+id
      $http.get url
    dataFactory.getPlayListByUsers = ()->
      user_id = window.localStorage['timetolisten_user_id']
      temporaire = window.localStorage['timetolisten_user_temporaire']
      token = window.localStorage['timetolisten_user_token']

     
      
      url = urlPlaylist+"?user_id="+user_id+"&temporaire="+temporaire+"&token="+token
      $http.get url
    

   

    dataFactory
]
