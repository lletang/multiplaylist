myApp = angular.module('timetolistenApp')

myApp.controller 'searchController', ['$scope','$routeParams', '$http', '$location','searchFactory','ngDialog', ($scope, $routeParams, $http, $location,searchFactory,ngDialog)->
	
	$scope.addMedia = ()->
		id =  window.localStorage['timetolisten_media.add.id']
		title =  window.localStorage['timetolisten_media.add.title'] 
		img =  window.localStorage['timetolisten_media.add.img'] 
		plateforme =  window.localStorage['timetolisten_media.add.plateforme'] 
		url = window.localStorage['timetolisten_media.add.url'] 

		$http {
			method: 'POST'
			url: '/api/medias/add'
			headers: 'Content-Type': 'application/x-www-form-urlencoded'
			transformRequest: (obj) ->
				str = []
				for p of obj
					str.push encodeURIComponent(p) + '=' + encodeURIComponent(obj[p])
				str.join '&'
			data: {id_playlist: $scope.playlist_id,media_id:id,media_title:title,media_img:img,media_plateforme:plateforme,url:url}
		}
		.success((data)->
			
			ngDialog.close()
			

		).error((error)->
			console.log error
		)




	$scope.RechercheMedia = (plateforme)->
		if plateforme == "youtube"
			searchFactory.getYoutube($scope.motcle).success((custs)->
				$scope.medias_youtube = custs.items
				$scope.medias_soundcloud = ""
				window.localStorage['timetolisten_media.add.plateforme']= "youtube"

				console.log custs

			).error (error)->
				console.log error+" erreur youtube"
		else if plateforme == "soundcloud"
			searchFactory.getSoundcloud($scope.motcle).success((custs)->
				$scope.medias_youtube = ""
				$scope.medias_soundcloud = custs
				window.localStorage['timetolisten_media.add.plateforme']= "soundcloud"
				

			).error (error)->
				console.log error+" erreur youtube"


	searchFactory.getPlayListByUsers().success((custs) ->

	  $scope.playlists = custs
	  
	  
	  
	).error (error) ->
	  console.log "error youtube get"

	$scope.open = (title,id,img,url) ->
		window.localStorage['timetolisten_media.add.id'] = id
		window.localStorage['timetolisten_media.add.title'] = title
		window.localStorage['timetolisten_media.add.img']  = img
		window.localStorage['timetolisten_media.add.url']  = url
		
		ngDialog.open({ template: 'firstDialogId', controller: 'searchController' });
			
	
	
	
			

]


