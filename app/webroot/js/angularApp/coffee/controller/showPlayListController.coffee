myApp = angular.module('timetolistenApp')

myApp.controller 'showPlayListController', ['$scope','$rootScope','$routeParams', '$http', '$location','searchFactory','ngDialog', ($scope, $rootScope, $routeParams, $http, $location, searchFactory, ngDialog)->
	
	$scope.libellePL = ''


	searchFactory.getListPlayList().success((data) -> 
    	$scope.objects = data
    ).error (error) -> 
    	console.log "error playlist get"




	UpdateListPlayList = ()->
	 	searchFactory.getPlayListByUsers().success((custs) ->
	  		$rootScope.playlists= custs
	  		$rootScope.$apply
	  		
	  	).error (error) ->
	  		console.log "error playlist get"


	UpdateListPlayList()

	
	$scope.addPL = () ->

		user_id = window.localStorage['timetolisten_user_id']
		temporaire = window.localStorage['timetolisten_user_temporaire']
		name = $("#nomPL").val()

		if user_id == undefined

			if temporaire == undefined
				id = "NULL"
				tmp = "NULL"

			else

				tmp = temporaire
				id = "NULL"
		else
			if temporaire != undefined
				tmp = temporaire
			else tmp = "NULL"
			id = user_id
			

		
		$http {
			method: 'POST'
			url: '/api/playlists/add'
			headers: 'Content-Type': 'application/x-www-form-urlencoded'
			transformRequest: (obj) ->
				str = []
				for p of obj
					str.push encodeURIComponent(p) + '=' + encodeURIComponent(obj[p])
				str.join '&'
			data: {nom: name,user_id:id,temporaire:tmp}
		}
		.success((data)->
			UpdateListPlayList()
			
			ngDialog.close()
			
			
			if data.utilisateur_id != undefined
				window.localStorage['timetolisten_user_id'] = data.utilisateur_id
				window.localStorage['timetolisten_user_temporaire'] = undefined
			
			if data.temporaire_id != undefined
				window.localStorage['timetolisten_user_temporaire'] = data.temporaire_id
				window.localStorage['timetolisten_user_id'] = undefined

			
			$location.path("/listeLecture/"+data.id)
		).error((error)->
			console.log error
		)
		return

	$scope.add = ->
  		ngDialog.open
    		template: '<div class="ngdialog-message"><div ng-controller="showPlayListController"><h3>Nouvelle playlist : </h3><input id="nomPL" name="libellePL" type="text" style="width:100%" ng-model="libellePL" /></div><div class="ngdialog-buttons"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="addPL()">Ajouter la playlist</button></div></div>'
    		plain: true
    		scope: $scope
		return

	return
			

]