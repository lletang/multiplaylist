myApp = angular.module('timetolistenApp')

myApp.controller 'playController', ['$scope','$rootScope','$routeParams', '$http', '$location','searchFactory','ngDialog', ($scope,$rootScope, $routeParams, $http, $location,searchFactory,ngDialog)->
	

	
    	
	
	soundcloud.addEventListener 'onPlayerReady', (player, data) ->
  		player.api_play()
  		return
	soundcloud.addEventListener 'onMediaEnd', (player, data) ->
  		$scope.next()
  		player.api_play()
  		return
  	
	$scope.play = (idPlayList)->

  		$('.jp-pause').removeClass 'hid'
  		$('.jp-play').addClass 'hid'
  		
  		$player = $('<div id="player"></div>')
  		etat = window.localStorage['timetolisten_etat_lecture']

	  	#si on clique sur le bouton du player, check si y'a deja une liste de charger en pause
	  	if idPlayList == undefined

	  		#si en pause
	  		if etat == "2"
		  		plateforme = window.localStorage['timetolisten_plateforme']
		  		#check plateforme youtube
		  		if plateforme == "1"
		  			iframe = document.getElementsByTagName("iframe")[0].contentWindow;
		  			func = 'playVideo';
		  			iframe.postMessage('{"event":"command","func":"' + func + '","args":""}','*');
		  			
		  		#check soundcloud
		  		else if plateforme == "2"
		  			pl = soundcloud.getPlayer 'PLAYER'
		  			pl.api_play()

		  		#on passe la variable de local storage de l'etat de la lecture en mode lecture
		  		window.localStorage['timetolisten_etat_lecture'] = 1
		  	#si sa reprend depuis le debut
		  	else
		  		listeEncours = window.localStorage['timetolisten_liste_en_cours']
		  		searchFactory.getOnePlayList(listeEncours).success((custs)->
	  				compteur = window.localStorage['timetolisten_compteur']
	  				$rootScope.playListeEnCours = custs
	  				$rootScope.$apply

	  				
	  				#si c'est youtube
	  				if custs.Media[compteur]["plateforme_id"]=="1"

	  						window.localStorage['timetolisten_plateforme'] = 1
	  						#création du lecteur YT
	  						$('#playerMusic').append $player
	  						player1 = new (YT.Player)('player',
	  						height: '390'
	  						width: '640'
	  						videoId: custs.Media[compteur]["id_media"]
	  						events:
	  							'onReady': onPlayerReady
	  							'onStateChange': onPlayerStateChange
		      			)
	  				#cas où c'est soundcloud
	  				else if custs.Media[compteur]["plateforme_id"]=="2"

	  					window.localStorage['timetolisten_plateforme'] = 2
	  					#création du lecteur soundcloud
	  					$player = $('<div id="player"></div>')
	  					$player.html '<object height="81" width="100%" id="PLAYER" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"><embed allowscriptaccess="always" height="81" src="http://player.soundcloud.com/player.swf?url='+custs.Media[compteur]["url"]+'&enable_api=true&object_id=PLAYER" type="application/x-shockwave-flash" width="100%" name="PLAYER"></embed></object>'
	  					$('#playerMusic').append $player

	  				#on met à jours le titre 
	  				$rootScope.titreEnCours= custs.Media[compteur]["titre"]


	  			)

  		

	  	else
	  		$('#player').remove()
	  		compteur = window.localStorage['timetolisten_compteur']
	  		etatLecture = window.localStorage['timetolisten_etat_lecture']
	  		listeEncours = window.localStorage['timetolisten_liste_en_cours']
	  		plateforme = window.localStorage['timetolisten_plateforme']

	  		#si premiere lecture sur le site initialisation des variables
	  		if etatLecture==undefined
	  			console.log "ici"
	  			window.localStorage['timetolisten_etat_lecture'] = 1
	  			searchFactory.getOnePlayList(idPlayList).success((custs)->
	  				$scope.playListeEnCours = custs
	  				$scope.$apply
	  			)
	  			window.localStorage['timetolisten_liste_en_cours'] = idPlayList
	  			window.localStorage['timetolisten_compteur'] = 0
	  			window.localStorage['timetolisten_id_playlist'] = idPlayList
	  		
	  	
	  		#si la playlist qu'on essaye de lire n'est pas la même que celle en cours, on charge le nouvelle PL
	  		if listeEncours!=idPlayList
	  			window.localStorage['timetolisten_compteur'] = 0

	  			#on charge la PL
	  			searchFactory.getOnePlayList(idPlayList).success((custs)->
	  			
	  				$rootScope.playListeEnCours = custs
	  				$rootScope.$apply

	  				window.localStorage['timetolisten_liste_en_cours'] = idPlayList
	  				#si c'est youtube
	  				if custs.Media[0]["plateforme_id"]=="1"

	  						window.localStorage['timetolisten_plateforme'] = 1
	  						#création du lecteur YT
	  						$('#playerMusic').append $player
	  						player1 = new (YT.Player)('player',
	  						height: '390'
	  						width: '640'
	  						videoId: custs.Media[0]["id_media"]
	  						events:
	  							'onReady': onPlayerReady
	  							'onStateChange': onPlayerStateChange
		      			)
	  				#cas où c'est soundcloud
	  				else if custs.Media[0]["plateforme_id"]=="2"

	  					window.localStorage['timetolisten_plateforme'] = 2
	  					#création du lecteur soundcloud
	  					$player = $('<div id="player"></div>')
	  					$player.html '<object height="81" width="100%" id="PLAYER" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"><embed allowscriptaccess="always" height="81" src="http://player.soundcloud.com/player.swf?url='+custs.Media[0]["url"]+'&enable_api=true&object_id=PLAYER" type="application/x-shockwave-flash" width="100%" name="PLAYER"></embed></object>'
	  					$('#playerMusic').append $player

	  				#on met à jours le titre 
	  				$rootScope.titreEnCours= custs.Media[0]["titre"]


	  			)#mettre si erreur
			#si c'est egal et donc la même qu'on veut réécouter
	  		else

	  			searchFactory.getOnePlayList(idPlayList).success((custs)->
	  				compteur = window.localStorage['timetolisten_compteur']
	  				$rootScope.playListeEnCours = custs
	  				$rootScope.$apply

	  				
	  				#si c'est youtube
	  				if custs.Media[compteur]["plateforme_id"]=="1"

	  						window.localStorage['timetolisten_plateforme'] = 1
	  						#création du lecteur YT
	  						$('#playerMusic').append $player
	  						player1 = new (YT.Player)('player',
	  						height: '390'
	  						width: '640'
	  						videoId: custs.Media[compteur]["id_media"]
	  						events:
	  							'onReady': onPlayerReady
	  							'onStateChange': onPlayerStateChange
		      			)
	  				#cas où c'est soundcloud
	  				else if custs.Media[compteur]["plateforme_id"]=="2"

	  					window.localStorage['timetolisten_plateforme'] = 2
	  					#création du lecteur soundcloud
	  					$player = $('<div id="player"></div>')
	  					$player.html '<object height="81" width="100%" id="PLAYER" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"><embed allowscriptaccess="always" height="81" src="http://player.soundcloud.com/player.swf?url='+custs.Media[compteur]["url"]+'&enable_api=true&object_id=PLAYER" type="application/x-shockwave-flash" width="100%" name="PLAYER"></embed></object>'
	  					$('#playerMusic').append $player

	  				#on met à jours le titre 
	  				$rootScope.titreEnCours= custs.Media[compteur]["titre"]


	  			)
	  			#on charge la PL
	  			console.log "lameme"

  	$scope.pause = ->
  		$('.jp-play').removeClass 'hid'
  		$('.jp-pause').addClass 'hid'
  		window.localStorage['timetolisten_etat_lecture'] = 2
  		plateforme = window.localStorage['timetolisten_plateforme']
  		if plateforme == "1"
  			console.log document.getElementsByTagName("iframe")
  			iframe = document.getElementsByTagName("iframe")[0].contentWindow;
  			func = 'pauseVideo' #: 'playVideo';
  			iframe.postMessage('{"event":"command","func":"' + func + '","args":""}','*');
  			

  		else if plateforme == "2"
  			pl = soundcloud.getPlayer 'PLAYER'
  			pl.api_pause()
  		return

  	#si bouton suivant
  	$scope.next = ->
  		nombreTotalMusic =  $rootScope.playListeEnCours.Media.length
  		
  		compteur = window.localStorage['timetolisten_compteur']
  		compteur = parseInt(compteur)+1
  		#si le compteur est en fin de music on le remet au debut
  		if compteur > nombreTotalMusic-1
  			window.localStorage['timetolisten_compteur'] = 0
  			compteur = 0
  		else
  			window.localStorage['timetolisten_compteur'] = compteur


  	
  		if parseInt($rootScope.playListeEnCours.Media[compteur]["plateforme_id"])==1
  			id = $rootScope.playListeEnCours.Media[compteur]["id_media"]
  		
  			changeMusic("youtube",id)
  		else
  			id = $rootScope.playListeEnCours.Media[compteur]["url"]
  			changeMusic("soundcloud",id)

  		$rootScope.titreEnCours= $rootScope.playListeEnCours.Media[compteur]["titre"]


  		
  		return
  	#si bouton precédent
  	$scope.prev = ->
  		nombreTotalMusic =  $rootScope.playListeEnCours.Media.length
  		
  		compteur = window.localStorage['timetolisten_compteur']
  		compteur = parseInt(compteur)-1

  		#si le compteur est en debut de music on le remet au debut
  		if compteur < 0
  			window.localStorage['timetolisten_compteur'] = nombreTotalMusic-1
  			compteur = nombreTotalMusic-1
  		else
  			window.localStorage['timetolisten_compteur'] = compteur


  	
  		if parseInt($rootScope.playListeEnCours.Media[compteur]["plateforme_id"])==1
  			id = $rootScope.playListeEnCours.Media[compteur]["id_media"]

  			changeMusic("youtube",id)
  		else
  			id = $rootScope.playListeEnCours.Media[compteur]["url"]
  			changeMusic("soundcloud",id)

  		$rootScope.titreEnCours= $rootScope.playListeEnCours.Media[compteur]["titre"]
  		
  		return

	changeMusic = (type,id) ->
  		if type == 'youtube'
    		$('#player').remove()
    		$player = $('<div id="player"></div>')
    		window.localStorage['timetolisten_plateforme'] = 1
    		$('#playerMusic').append $player
    		player1 = new (YT.Player)('player',
	      		height: '390'
	      		width: '640'
	      		videoId: id
	      		events:
	        		'onReady': onPlayerReady
	        		'onStateChange': onPlayerStateChange)
  		else if type == 'soundcloud'
    		#alert("soundcloud");
    		window.localStorage['timetolisten_plateforme'] = 2
    		$('#player').remove()
    		$player = $('<div id="player"></div>')
    		$player.html '<object height="81" width="100%" id="PLAYER" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"><embed allowscriptaccess="always" height="81" src="http://player.soundcloud.com/player.swf?url='+id+'&enable_api=true&object_id=PLAYER" type="application/x-shockwave-flash" width="100%" name="PLAYER"></embed></object>'
    		$('#playerMusic').append $player
  		return
  	$scope.setSound = ->
  		volume = $('#volume-sound').val()

  		plateforme = window.localStorage['timetolisten_plateforme']
  		if plateforme == "1"
  			
  			iframe = document.getElementsByTagName("iframe")[0].contentWindow;
  			func = 'setVolume' #: 'playVideo';
  			iframe.postMessage('{"event":"command","func":"' + func + '","args":['+volume+']}','*');
  			

  		else if plateforme == "2"
  			pl = soundcloud.getPlayer 'PLAYER'
  			pl.api_setVolume(volume)

  		return
  	# autoplay video

	onPlayerReady = (event) ->
		event.target.playVideo()
		return

	# when video ends for youtube

	onPlayerStateChange = (event) ->
		if event.data == 0
			$scope.next()
		return

	
]