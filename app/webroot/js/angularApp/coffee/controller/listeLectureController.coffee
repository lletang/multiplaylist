myApp = angular.module('timetolistenApp')

myApp.controller 'listeLectureController', ['$scope','$rootScope','$routeParams', '$http', '$location','searchFactory','ngDialog', ($scope,$rootScope, $routeParams, $http, $location,searchFactory,ngDialog)->
	
	$scope.idPL = $routeParams.id
	$scope.$apply

	$scope.listMedia = []

	
	searchFactory.getOnePlayList($routeParams.id).success((custs)->
		$scope.PlayListPage = custs
		
		angular.forEach(custs.Media, (media)->
			$scope.listMedia.push media
		)
	)

	$scope.deleteMusic = (media)->
		$http.delete('/api/playlists/media/'+media.id).success((data)->
			if data.success != undefined 
				$scope.successDel = "Media supprimé"
			if data.error != undefined
				$scope.errorDel = "Le media n'a pas pu être supprimé"
		).error((error)->
			console.log error
		)

	$scope.changePrivacy = (idPL)->
		if $('#checkBoxPrivacy').is(':checked') == true
			position = 1
		else position = 0 

		$http.get('/api/playlists/privacy/'+position+"/"+idPL).success((data)->
			if data.position == 1
				po = true
			else 
				po = false

			
			$('#checkBoxPrivacy').prop('checked');
			#$('#checkBoxPrivacy').prop("checked",po)
		).error((error)->
			console.log error
		)

]