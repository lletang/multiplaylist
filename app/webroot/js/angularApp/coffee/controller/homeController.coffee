myApp = angular.module('timetolistenApp')

myApp.controller 'homeController', ['$scope', '$rootScope', '$routeParams', '$http', '$location','searchFactory', ($scope, $rootScope, $routeParams, $http, $location,searchFactory)->

	$rootScope.session = false

	$rootScope.hasSession = ()->
		$http {
			method: 'POST'
			url: '/api/user/session'
			headers: 'Content-Type': 'application/x-www-form-urlencoded'
			transformRequest: (obj) ->
				str = []
				for p of obj
					str.push encodeURIComponent(p) + '=' + encodeURIComponent(obj[p])
				str.join '&'
			data: {token: window.localStorage['timetolisten_user_token']}
		}
		.success((data)->
			if data.connected != undefined
				$rootScope.session = true
			else
				$rootScope.session = false
			
		).error((error)->
			console.log error
		)

]