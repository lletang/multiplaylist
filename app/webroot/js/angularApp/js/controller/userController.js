(function() {
  var myApp;
  myApp = void 0;
  myApp = angular.module('timetolistenApp');
  myApp.controller('userController', [
    '$scope', '$rootScope', '$routeParams', '$http', '$location', 'searchFactory', function($scope, $rootScope, $routeParams, $http, $location, searchFactory) {
      var UpdateListPlayList;
      UpdateListPlayList = void 0;
      UpdateListPlayList = function() {
        return searchFactory.getPlayListByUsers().success(function(custs) {
          $rootScope.playlists = custs;
          return $rootScope.$apply;
        }).error(function(error) {
          return console.log('error playlist get');
        });
      };
      $scope.logout = function() {
        var id_user;
        id_user = void 0;
        id_user = window.localStorage['timetolisten_user_id'];
        return $http({
          method: 'POST',
          url: '/api/users/logout',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          transformRequest: function(obj) {
            var p, str;
            p = void 0;
            str = void 0;
            str = [];
            for (p in obj) {
              p = p;
              str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
            }
            return str.join('&');
          },
          data: {
            id: id_user
          }
        }).success(function(data) {
          window.localStorage['timetolisten_user_id'] = void 0;
          window.localStorage['timetolisten_user_token'] = 'NULL';
          UpdateListPlayList();
          $rootScope.nomUtilisateur = '';
          $rootScope.$apply;
          return $location.path('/');
        }).error(function(error) {
          return console.log(error);
        });
      };
      $scope.login = function() {
        if ($scope.user.email == void 0) {
          $scope.user.email = '';
        }
        if ($scope.user.password == void 0) {
          $scope.user.password = '';
        }
        return $http({
          method: 'POST',
          url: '/api/users/login',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          transformRequest: function(obj) {
            var p, str;
            p = void 0;
            str = void 0;
            str = [];
            for (p in obj) {
              p = p;
              str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
            }
            return str.join('&');
          },
          data: {
            email: $scope.user.email,
            password: $scope.user.password
          }
        }).success(function(data) {
          if (data.success != void 0) {
            window.localStorage['timetolisten_user_id'] = data.utilisateur_id;
            $rootScope.nomUtilisateur = 'Bonjour ' + data.login;
            $rootScope.$apply;
            window.localStorage['timetolisten_user_token'] = data.token;
            UpdateListPlayList();
            return $location.path('/');
          } else {
            $scope.errorEmail = '';
            if (data.error != void 0) {
              return $scope.errorEmail = data.error;
            }
          }
        }).error(function(error) {
          return console.log(error);
        });
      };
      $scope.register = function() {
        if ($scope.user.login == void 0) {
          $scope.user.login = '';
        }
        if ($scope.user.email == void 0) {
          $scope.user.email = '';
        }
        if ($scope.user.password == void 0) {
          $scope.user.password = '';
        }
        return $http({
          method: 'POST',
          url: '/api/users/register',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          transformRequest: function(obj) {
            var p, str;
            p = void 0;
            str = void 0;
            str = [];
            for (p in obj) {
              p = p;
              str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
            }
            return str.join('&');
          },
          data: {
            login: $scope.user.login,
            email: $scope.user.email,
            password: $scope.user.password
          }
        }).success(function(data) {
          if (data.success != void 0) {
            return $location.path('/login');
          } else {
            $scope.errorPassword = '';
            $scope.errorLogin = '';
            $scope.errorEmail = '';
            if (data.password != void 0) {
              $scope.errorPassword = data.password['0'];
            }
            if (data.login != void 0) {
              $scope.errorLogin = data.login['0'];
            }
            if (data.email != void 0) {
              return $scope.errorEmail = data.email['0'];
            }
          }
        }).error(function(error) {
          return console.log(error);
        });
      };
      return $scope.editProfil = function() {
        var id_user;
        id_user = void 0;
        id_user = window.localStorage['timetolisten_user_id'];
        if ($scope.user.login == void 0) {
          $scope.user.login = '';
        }
        if ($scope.user.email == void 0) {
          $scope.user.email = '';
        }
        if ($scope.user.password == void 0) {
          $scope.user.password = '';
        }
        return $http({
          method: 'PUT',
          url: '/api/users/update',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          transformRequest: function(obj) {
            var p, str;
            p = void 0;
            str = void 0;
            str = [];
            for (p in obj) {
              p = p;
              str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
            }
            return str.join('&');
          },
          data: {
            id: id_user,
            login: $scope.user.login,
            email: $scope.user.email,
            password: $scope.user.password
          }
        }).success(function(data) {
          if (data.success != void 0) {
            $rootScope.nomUtilisateur = 'Bonjour ' + data.login;
            $rootScope.$apply;
            return $scope.success = 'success';
          } else {
            $scope.errorPassword = '';
            $scope.errorLogin = '';
            $scope.errorEmail = '';
            if (data.password != void 0) {
              $scope.errorPassword = data.password['0'];
            }
            if (data.login != void 0) {
              $scope.errorLogin = data.login['0'];
            }
            if (data.email != void 0) {
              return $scope.errorEmail = data.email['0'];
            }
          }
        }).error(function(error) {
          return console.log(error);
        });
      };
    }
  ]);
}).call(this);

// ---
// generated by coffee-script 1.9.0