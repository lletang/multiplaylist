<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/api/youtube/search/:motcle', array('controller' => 'Youtube', 'action' => 'search'));
	Router::connect('/api/soundcloud/search/:motcle', array('controller' => 'Soundcloud', 'action' => 'search'));
	Router::connect('/api/playlists/add', array('controller' => 'PlayList', 'action' => 'add'));
	Router::connect('/api/playlists/:id', array('controller' => 'PlayList', 'action' => 'playlists'));
	Router::connect('/api/playlists', array('controller' => 'PlayList', 'action' => 'playlists'));
	Router::connect('/api/playlist/search', array('controller' => 'PlayList', 'action' => 'search'));
	
	Router::connect('/api/playlists/privacy/:position/:idPL', array('controller' => 'PlayList', 'action' => 'privacy'));
	Router::connect('/api/playlists/media/:id', array('controller' => 'Media', 'action' => 'delete'));
	Router::connect('/api/medias/add', array('controller' => 'Media', 'action' => 'add'));
	Router::connect('/api/users/register', array('controller' => 'Users', 'action' => 'register'));
	Router::connect('/api/users/login', array('controller' => 'Users', 'action' => 'login'));
	Router::connect('/api/users/logout', array('controller' => 'Users', 'action' => 'logout'));
	Router::connect('/api/users/update', array('controller' => 'Users', 'action' => 'update'));

	Router::connect('/api/user/session', array('controller' => 'Users', 'action' => 'session'));


// Index
	Router::connect('/', array('controller' => 'dashboard', 'action' => 'index', 'home'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));



/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
